using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Wappa.CaptureRideService.Entities;
using Wappa.CaptureRideService.Repositories;
using Wappa.CaptureRideService.SQS;
using Wappa.Framework.Cache;

namespace Wappa.CaptureRideService.Tasks
{
    public class RidePendingCaptureTask : BackgroundService
    {
        private readonly IRidePendingCaptureRepository _ridePendingCaptureRepository;
        private readonly RidePendingCaptureOptions _options;
        private readonly ICache _cache;
        private readonly IEventPublisher _publisher;
        private readonly ILogger<RidePendingCaptureTask> _logger;

        public RidePendingCaptureTask(RidePendingCaptureOptions options,
            IRidePendingCaptureRepository ridePendingCaptureRepository,
            ICache cache, IEventPublisher publisher, ILogger<RidePendingCaptureTask> logger)
        {
            _options = options;
            _ridePendingCaptureRepository = ridePendingCaptureRepository;
            _cache = cache;
            _publisher = publisher;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _logger.LogDebug("RidePendingCapture is starting.");

                stoppingToken.Register(() => _logger.LogDebug("RidePendingCapture background task is stopping."));

                var rides = await _ridePendingCaptureRepository.GetRidesPendingCaptureAsync();

                Parallel.ForEach(rides, async ride => { await FilterReasonTypeAsync(ride); });

                _logger.LogDebug($"RidePendingCapture background task is stopping.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "There was an error processing rides pending capture.");
                throw;
            }
        }

        private async Task FilterReasonTypeAsync(
            RidePendingCaptureEntity entity)
        {
            var type = GetValueFromDescription<ReasonType>(entity.Reason.ToUpper());

            var executionManager = await GetCache(entity.RideId) ?? new ExecutionManager
            {
                ReasonType = type,
                NextExecution = DateTime.Now
            };

            if (executionManager.NextExecution.Date == DateTime.Now.Date
                && !entity.BlockPaymentReprocessing)
            {
                switch (executionManager.ReasonType)
                {
                    case ReasonType.Refused:
                    case ReasonType.Fraud:
                    case ReasonType.BlockedCard:
                    case ReasonType.NotEnoughBalance:
                    case ReasonType.DeclinedNonGeneric:
                    case ReasonType.InvalidAmount:
                    case ReasonType.WithdrawalCountExceeded:
                    case ReasonType.TransactionNotPermitted:
                    case ReasonType.IssuerUnavailable:
                    case ReasonType.Notsupported:
                        if (executionManager.AttemptsExecution >= _options.FirstExecutionGroupPerDay)
                        {
                            executionManager.NextExecution =
                                executionManager.NextExecution.AddDays(_options.FirstExecutionGroupInDays);
                            executionManager.AttemptsExecution = 0;
                            await SetCache(executionManager);
                            break;
                        }

                        await ProcessPaymentAsync(executionManager, entity);
                        break;
                    case ReasonType.InvalidCardNumber:
                    case ReasonType.ExpiredCard:
                    case ReasonType.RestrictedCard:
                    case ReasonType.AcquirerError:
                    case ReasonType.CvcDeclined:
                    case ReasonType.Referral:
                    case ReasonType.ServiceException:
                        if (executionManager.AttemptsExecution >= _options.SecondExecutionGroupPerDay)
                        {
                            executionManager.NextExecution =
                                executionManager.NextExecution.AddDays(_options.SecondExecutionGroupInDays);
                            executionManager.AttemptsExecution = 0;
                            await SetCache(executionManager);
                            break;
                        }

                        await ProcessPaymentAsync(executionManager, entity);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private async Task ProcessPaymentAsync(ExecutionManager executionManager,
            RidePendingCaptureEntity ridePendingCaptureEntity)
        {
            _logger.LogInformation($"Processing payment for RideId: {executionManager.RideId}");

            try
            {
                await _publisher.PublishEvent(new
                {
                    ridePendingCaptureEntity.PaymentOrderId,
                    ridePendingCaptureEntity.UserId
                });

                executionManager.AttemptsExecution += 1;
                executionManager.LastExecution = DateTime.UtcNow;
                executionManager.PaymentOrderId = ridePendingCaptureEntity.PaymentOrderId;
                executionManager.UserId = ridePendingCaptureEntity.UserId;
                executionManager.RideId = ridePendingCaptureEntity.RideId;

                await SetCache(executionManager);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,
                    $"FAILED  PaymentOrderId {ridePendingCaptureEntity.PaymentOrderId} UserId: {ridePendingCaptureEntity.UserId}");
                throw;
            }
        }

        private static string GetCacheKey(long rideId)
        {
            return $"{nameof(CaptureRideService)}_{rideId}";
        }

        private async Task SetCache(ExecutionManager executionManager)
        {
            var key = GetCacheKey(executionManager.RideId);
            await _cache.SetAsync(key, executionManager);
        }

        private async Task<ExecutionManager> GetCache(long rideId)
        {
            var key = GetCacheKey(rideId);
            var cache = await _cache.GetAsync<ExecutionManager>(key);

            return cache;
        }

        private static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException($"Type {nameof(T)} not enum");

            foreach (var field in type.GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description == description)
                        return (T) field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T) field.GetValue(null);
                }
            }

            throw new ArgumentException("Not found.", description);
        }
    }
}