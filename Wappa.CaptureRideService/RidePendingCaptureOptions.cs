namespace Wappa.CaptureRideService
{
    public class RidePendingCaptureOptions
    {
        public int FirstExecutionGroupInDays { get; set; }
        public int FirstExecutionGroupPerDay { get; set; }

        public int SecondExecutionGroupInDays { get; set; }
        public int SecondExecutionGroupPerDay { get; set; }
        public string QueueEndPoint { get; set; }
    }
}