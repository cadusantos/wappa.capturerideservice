using System;
using System.Threading.Tasks;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Wappa.CaptureRideService.SQS
{
    public class SqsEventPublisher : IEventPublisher
    {
        private readonly RidePendingCaptureOptions _options;
        private readonly IAmazonSQS _sqsClient;
        private readonly ILogger<SqsEventPublisher> _logger;

        public SqsEventPublisher(RidePendingCaptureOptions options, IAmazonSQS sqsClient,
            ILogger<SqsEventPublisher> logger)
        {
            _options = options;
            _sqsClient = sqsClient;
            _logger = logger;
        }

        public async Task PublishEvent<T>(T message)
        {
            try
            {
                var request = new SendMessageRequest(_options.QueueEndPoint,
                    JsonConvert.SerializeObject(message));

                await _sqsClient.SendMessageAsync(request);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Couldn't publish an event to SQS");
                throw;
            }
        }
    }
}