﻿using Amazon.SQS;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Wappa.CaptureRideService.Repositories;
using Wappa.CaptureRideService.SQS;
using Wappa.CaptureRideService.Tasks;
using Wappa.Framework.Cache;
using Wappa.Framework.Cache.Redis;
using Wappa.Framework.Cache.Redis.Handler;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Wappa.CaptureRideService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions<RidePendingCaptureOptions>()
                .Bind(Configuration.GetSection(nameof(RidePendingCaptureOptions)))
                .ValidateDataAnnotations();

            services.AddSingleton<IHostedService, RidePendingCaptureTask>();

            services.AddSingleton<IRidePendingCaptureRepository>(_ =>
                new RidePendingCaptureRepository(Configuration.GetConnectionString("Postgres")));

            services.AddTransient(provider => provider.GetRequiredService<IOptions<RidePendingCaptureOptions>>().Value);
            
            services.AddSingleton<ICache, RedisCache>();
            services.AddScoped<IDatabaseHandler, RedisDatabaseHandler>();
            services.AddScoped<IRedisDatabaseProvider, RedisDatabaseProvider>();
            services.AddSingleton<IRedisDatabaseOptions>(redis =>
                new RedisDatabaseOptions(Configuration, "Redis:ConnectionString",
                    "Redis:DatabaseId"));
            services.AddScoped<ILazyCache, LazyCache>();
            services.AddScoped<IWriteThroughCache, WriteThroughCache>();


            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAWSService<IAmazonSQS>();
            services.AddSingleton<IEventPublisher, SqsEventPublisher>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}