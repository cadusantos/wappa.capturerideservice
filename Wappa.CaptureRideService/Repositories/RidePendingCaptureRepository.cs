using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Npgsql;
using Wappa.CaptureRideService.Entities;

namespace Wappa.CaptureRideService.Repositories
{
    public class RidePendingCaptureRepository : IRidePendingCaptureRepository
    {
        private readonly string _connectionString;

        public RidePendingCaptureRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IEnumerable<RidePendingCaptureEntity>> GetRidesPendingCaptureAsync()
        {
            const string query = @"select distinct on (dr.id) UPPER(pa.refusalreason) reason,
                                                             rd.rideid,
                                                              dr.id           PaymentOrderId,
                                                              rd.userid       UserId,
                                                              ord.status      PaymentStatus,
                                                              rd.status       RideStatus,                                                              
                                                              max(pa.createdat),
                                                              pd.blockpaymentreprocessing
                                   
                                   
                                   from rideb2c.pendingcapture pd
                                            inner join rideb2c.ride rd on
                                       pd.rideid = rd.id
                                            inner join personal.account ac on
                                       rd.userid = ac.userid
                                            inner join rideb2c.paymentorder ord on
                                       ord.rideid = rd.id
                                            inner join payment.paymentorder dr on
                                       dr.paymentmethodid = ord.paymentmethod
                                            inner join payment.authorize pa on
                                           pa.paymentorderid = ord.authorize
                                           and dr.status = 'PendingCapture'
                                   where rd.status = 'Completed'
                                     and isdeleted = false
                                     and ac.status = 'Active'
                                     and ord.status != 'Captured'
                                     and pa.refusalreason <> ''
                                   group by dr.id,
                                            pa.refusalreason,
                                            rd.rideid,
                                            rd.userid,
                                            ord.status,
                                            rd.status,                                        
                                            pd.blockpaymentreprocessing";

            using (var connection = new NpgsqlConnection(_connectionString))
            {
                var result = await connection.QueryAsync<RidePendingCaptureEntity>(query);

                return result;
            }
        }
    }
}