using System.Collections.Generic;
using System.Threading.Tasks;
using Wappa.CaptureRideService.Entities;

namespace Wappa.CaptureRideService.Repositories
{
    public interface IRidePendingCaptureRepository
    {
        Task<IEnumerable<RidePendingCaptureEntity>> GetRidesPendingCaptureAsync();
    }
}