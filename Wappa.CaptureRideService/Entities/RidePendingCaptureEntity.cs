using System;

namespace Wappa.CaptureRideService.Entities
{
    public class RidePendingCaptureEntity
    {
        public long RideId { get; set; }
        public Guid PaymentOrderId { get; set; }
        public long UserId { get; set; }        
        public string Reason { get; set; }
        public bool BlockPaymentReprocessing { get; set; }
    }
}