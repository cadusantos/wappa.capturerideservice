using System;

namespace Wappa.CaptureRideService.Entities
{
    public class ExecutionManager
    {
        public DateTime LastExecution { get; set; }
        public ReasonType ReasonType { get; set; }
        public DateTime NextExecution { get; set; }
        public int AttemptsExecution { get; set; }
        public bool BlockedForExecution { get; set; }
        public Guid PaymentOrderId { get; set; }
        public long UserId { get; set; }
        public long RideId { get; set; }
    }
}